## Test environments
* local OS X install, R 3.6.0
* ubuntu 14.04 (on travis-ci), R 3.6.0
* win-builder (devel and release)

## R CMD check results

0 errors | 0 warnings | 0 note

* This is a maintenance release.

---

Google is changing the endpoint for the JSON REST API so
this updates accounts for that change. New datasets and
response helpers have also been added.
